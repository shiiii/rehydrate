import React, {Component} from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity, ImageBackground, Vibration, TextInput, FlatList } from 'react-native';
import Wave from 'react-native-waveview';
import {Icon} from 'native-base';
import Modal from "react-native-modal";
import AsyncStorage from '@react-native-community/async-storage';
import RNShake from 'react-native-shake';
import Notification from './Notification';


class Main extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            cupToUse: require('../UI/cup100.png'),
            intake: 100,
            isModalVisible: false,
            total: 0,
            dailyGoal: 0,
            age: 29,
            weight: 162,
            gotDateSaved: true,
            text: '',
            multiplier: 40,
            onPlayAnimation: false,
            percent: 0,
            waterLevel: 100,
            showNotification: false,
        };


        this.cups = [
            {"cup": require('../UI/coffee50.png'), "volume" : '50 ml', "value" : 50, "id": '0', "height": 60, "width": 60},
            {"cup": require('../UI/teacup50.png'), "volume" : '50 ml', "value" : 50, "id": '1', "height": 70, "width": 70},
            {"cup": require('../UI/juice.png'), "volume" : '70 ml', "value" : 70,"id": '2', "height": 80, "width": 80},
            {"cup": require('../UI/cup100.png'), "volume" : '100 ml', "value" : 100,"id": '3', "height": 55, "width": 55},
            {"cup": require('../UI/cup200.png'), "volume" : '150 ml', "value" : 150,"id": '4', "height": 60, "width": 60},
            {"cup": require('../UI/cup300.png'), "volume" : '175 ml', "value" : 175, "id": '5', "height": 65, "width": 65},
            {"cup": require('../UI/cup400.png'), "volume" : '200 ml', "value" : 200, "id": '6', "height": 70, "width": 70},
            {"cup": require('../UI/customize.png'),  "volume" : 'Customize', "value" : 250, "id": '7', "height": 70, "width": 70},
        ]
    }

    componentDidMount()
    {
        RNShake.addEventListener('ShakeEvent', () => {
            // Your code...
            this._waveRect && this._waveRect.startAnim();
            this.checkTimeout();
        });

        this.getVolumePerDay();
        this.calculateDailyGoalIntakeAndSave();
    }

    componentWillUnmount() {
        RNShake.removeEventListener('ShakeEvent');
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    
    _handleAgeValue (value)
    {
        this.setState({age: value});

        console.log(this.state.age);
    }

    _handleWeightValue (value)
    {
        this.setState({weight: value});

        console.log(this.state.weight);
    }


    calculateDailyGoalIntakeAndSave(){
        const val1 = this.state.weight / 2.2;
        if ( this.state.age < 30)
        {
            this.setState({multiplier: 40})
        }
        else if (this.state.age >= 30 && this.state.age <= 55)
        {
            this.setState({multiplier: 35})
        }
        else if (this.state.age > 55)
        {
            this.setState({multiplier: 30})
        }

        const val2 = (val1 * this.state.multiplier)
        const val22 = val2 / 28.3

        const liters = (val22 / 33.8)
        const ml = liters * 1000

        this.setState({dailyGoal: ml})    
        this.storeData('dailyGoal', this.state.dailyGoal.toString())
    }

    subtractLiquidIntake(){
        const val1 = this.state.dailyGoal - this.state.intake

        if (val1 < 0)
            this.setState({dailyGoal: 0})
        else
            this.setState({dailyGoal: val1})

        // this.getPercentPerIntake()

        const percent =  (this.state.intake / this.state.dailyGoal);

        const value = percent * 200 

        const value1 = this.state.waterLevel - value
        console.log("value1 : " + value1)
        this.setState({waterLevel: value1})
        console.log("waterLevel : " + this.state.waterLevel)

        
    }

    getVolumePerDay = async() =>
    {
        try {
            const value = await AsyncStorage.getItem('volumePerDay')
            .then ((res) => {
                console.log('res :' + res)
                if(res !== null) {
                    // value previously stored
                    this.setState({gotDateSaved: true, dailyGoal: res})
                }
            })
            .catch ((error) => console.log("Error :" + error));
          } catch(e) {
            // error reading value
          }
    }

   

    getData = async (item_key) => {
        try {
          const value = await AsyncStorage.getItem(item_key)
          if(value !== null) {
            // value previously stored
          }
        } catch(e) {
          // error reading value
        }
    }

    storeData = async (item_key, item_value) => {
        try {
            await AsyncStorage.setItem(item_key, item_value)
                // .then ((res) => {
                //     if ( res === null || res === undefined)
                //     {
                //         console.log('Saving data failed!')
                //     }
                //     else {
                //         console.log('Data saved!')
                //     }
                // })
                // .catch ( (error) =>console.log('Error : ' + error));
        } catch (e) {
          // saving error
        }
    }

    checkTimeout() {

        this._waveRect && this._waveRect.setWaterHeight(this.state.waterLevel)

        setTimeout(() => { 
            this._waveRect && this._waveRect.stopAnim()

                // reset wave effect

        }, 2000);
        this._waveRect && this._waveRect.setWaveParams([
            {A: 15, T: 230, fill: '#62c2ff'},
            {A: 10, T: 200, fill: '#0087dc'},
            {A: 20, T: 250, fill: '#1aa7ff'},
        ])
     
    }

    renderNotification()
    {
       this.setState({showNotification: true})
    }

    renderWaveAnimation(level){
        return(
            <View >
                <TouchableOpacity onPress = {() => { 
                    this._waveRect && this._waveRect.startAnim();
                    this.checkTimeout();
                }}>
                    <Wave
                        ref={ref=>this._waveRect = ref}
                        style={_styles.waveBall}
                        H={100}
                        speed = {15}
                        waveParams={[
                            {A: 15, T: 230, fill: '#62c2ff'},
                            {A: 10, T: 200, fill: '#0087dc'},
                            {A: 20, T: 250, fill: '#1aa7ff'},
                        ]}
                        animated={false} >
                    </Wave>
                </TouchableOpacity>
                
            </View>
        );
    }

    switchCup(item){
        this.setState({cupToUse: item.cup, intake: item.value})
        this.toggleModal()
    }
    
    selectAndSwitchToThisCup()
    {
        return (
            <View style = {{flex: 0.75, backgroundColor: 'white',  borderRadius: 20, opacity: 0.9}}>
                <View>
                    <Text style = {{alignSelf: 'flex-start', margin: 20, fontSize: 20, opacity: 0.8, fontWeight: 'bold' }}>
                        Switch cup
                    </Text>
                </View>
                <FlatList
                    style = {{alignSelf: 'center', width: 320, height: 300, }}
                    numColumns = {2}
                    data = {this.cups}
                    renderItem = {({item}) => 
                            <TouchableOpacity onPress = {this.switchCup.bind(this,item)} style = {{ marginTop:10, marginBottom: 10, marginLeft: 35, borderRadius: 20,  backgroundColor: 'white', shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34, shadowRadius: 6.27, elevation: 10,}}>
                                <View style = {{height: 100, width: 100, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', borderRadius: 20,}} >
                                    <Image style = {{height: item.height, width: item.width, borderRadius: 20, }} source = {item.cup}>
                                    </Image>
                                    <Text style = {{justifyContent: 'center'}}>
                                        {item.volume}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                    }
                    keyExtractor = {(item, index) => item.id}
                    
                ></FlatList>
                <View style = {{flex: 2,  justifyContent: 'flex-end'}}>
                    <TouchableOpacity style = {{alignSelf: 'flex-end', marginTop: 20, marginRight: 20, marginBottom: 10}} onPress = {this.toggleModal}>
                        <Text style = {{color: 'blue', fontSize: 20,}}>
                            Cancel
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    renderUserDateInput()
    {
        return (
            <View style = {{justifyContent: 'center', }}>
                <View style = {{ flexDirection: 'row',  margin: 30 }}>
                    <Text style = {{paddingRight: 10, marginRight: 17, alignSelf: 'center',}}>
                        Age : 
                    </Text>
                    <TextInput
                        style={{ justifyContent: 'center', alignSelf: 'center', height: 40, width: 50, borderColor: 'gray', borderWidth: 1 }}
                        keyboardType = {'numeric'}
                        maxLength = {2}
                        onChangeText={this._handleAgeValue.bind(this)} //(text) => this.setState({age: text})
                        value = {String(this.state.age)}
                        />
                </View>

                <View style = {{ flexDirection: 'row',  margin: 30}}>
                    <Text style = {{paddingRight: 10, alignSelf: 'center', }}>
                        weight : 
                    </Text>
                    <TextInput
                        style={{ justifyContent: 'center', alignSelf: 'center', height: 40, width: 50, borderColor: 'gray', borderWidth: 1,  }}
                        keyboardType = {'numeric'}
                        maxLength = {6}
                        onChangeText={this._handleWeightValue.bind(this)}
                        value = {String(this.state.weight)}
                    />
                </View>
                <View style = {{alignItems: 'center', }}>
                    <TouchableOpacity onPress = {this.calculateDailyGoalIntakeAndSave} style = {{width: 100, height: 50,  backgroundColor: 'green', alignItems: 'center', justifyContent: 'center', borderRadius: 10,}}>
                        <Text style = {{ alignSelf: 'center', color: 'white'}}>
                            Proceed
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    renderUserDailyGoal()
    {
        return (
            <View style = {{flex: 2.5, flexDirection: 'row', justifyContent: 'center', margin: 10, backgroundColor: 'white', borderRadius: 8, shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34, shadowRadius: 6.27, elevation: 10,}}>
                 {/* {this.renderCupToUse()} */}
                 {/* <View style = {{flexDirection: 'row', justifyContent: 'center', borderColor: 'gray', borderWidth: 1}}> */}
                    <View style = {{ flex: 0.5,justifyContent: 'center', alignSelf: 'flex-end', marginBottom: 30, marginLeft: 30, }}>
                        <TouchableOpacity style = {{ backgroundColor: 'white', height: 70, width: 70,  borderRadius: 35, justifyContent: 'center', alignSelf: 'center', shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34, shadowRadius: 6.27, elevation: 10,}} onPress = {this.subtractLiquidIntake.bind(this)}>
                            <Image style = {{ height: 40, width: 40 , alignSelf: 'center',}} source = {this.state.cupToUse}>
                            </Image>
                            <Text style = {{justifyContent: 'center', alignSelf: 'center', fontSize: 15}}>
                            {this.state.intake + " ml"}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    {/* <View style = {{height: 100, width: 55, justifyContent: 'center', alignSelf: 'center', backgroundColor: 'blue',  borderWidth: StyleSheet.hairlineWidth,  }}> */}
                    <View style = {{justifyContent: 'center', alignSelf: 'center',}}>
                        {/* <ImageBackground
                            style = {{width: 150, height: 300,}} 
                            source = {require('../UI/liquid_container.png')}>
                        </ImageBackground> */}
                         {this.renderWaveAnimation(this.state.waterLevel)}
                        <View style = {{ margin: 8,  justifyContent: 'center', flex: 0.8,}}>
                            <View style = {{alignSelf:'center', alignItems: 'center',}}>
                                <Text style = {{ fontWeight: '100', fontSize: 15,}}>
                                    Your daily goal is
                                </Text>
                                <Text style = {{fontWeight: 'bold', fontSize: 30,}}>
                                    {(this.state.dailyGoal).toFixed(1) + " ml"}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style = {{flex: 0.5, alignSelf: 'flex-end',  marginBottom: 30, marginRight: 30,  }}>
                        <TouchableOpacity onPress = {this.toggleModal}  style = {{flexDirection: 'row', backgroundColor: 'white', height: 60, width: 60,  borderRadius: 35, shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34, shadowRadius: 6.27, elevation: 10,  }}>
                            <ImageBackground style = {{ height: 60, width: 60, alignSelf: 'center', justifyContent: 'center' }} source = {require('../UI/customize.png')}>
                                <Image style = {{ marginTop: 10, alignSelf: 'center', height: 15, width: 15, }} source = {require('../UI/Arrow.png')}>
                                </Image>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                {/* </View> */}
            </View>
        )
    }

    render ()
    {
        if ( this.state.showNotification)
        {
            return <Notification></Notification>
        }

        return (
            <View style={_styles.container} >
               {/* {this.state.gotDateSaved ? this.renderUserDailyGoal() : this.renderUserDateInput()} */}
                <View style = {{marginTop: 10, marginLeft: 10,  marginRight: 10, flex: 0.8, flexDirection : 'row', justifyContent: 'center', alignItems: 'center', borderRadius: 10, backgroundColor:'white', shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34, shadowRadius: 6.27, elevation: 10,    }}>
                    <Image style = {{height: 80, width: 80, marginRight: 10,  marginLeft: 30}} source = {require('../UI/water-drop.png')}>
                    </Image>
                    <Text style = {{fontWeight: 'bold', fontSize: 15, marginRight: 30}}>
                       {'Water is the Best Natural Remedy.\n Drink Your Way to Better Health.'}
                       {/* {'Keep calm, Dring water'} */}
                    </Text>
                </View>
               {this.renderUserDailyGoal()}
                <View style = {{ marginBottom: 10, marginLeft: 20,  marginRight: 20,  flex: 0.7, flexDirection : 'row', justifyContent: 'center', alignItems: 'center', }}>
                    <TouchableOpacity style = {{ margin: 8, backgroundColor: 'white', height: 70, width: 70,  borderRadius: 45, justifyContent: 'center', alignSelf: 'center', shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34, shadowRadius: 6.27, elevation: 10,}}>
                        {/* <Image style = {{ height: 50, width: 50 , alignSelf: 'center',}} source = {this.state.cupToUse}>
                        </Image> */}
                        {/* <Text style = {{ margin: 10,justifyContent: 'center', alignSelf: 'center', fontSize: 15}}>
                        {'Daily \nrecords'}
                        </Text> */}
                        <Image style = {{ justifyContent: 'center', alignSelf: 'center', height: 45, width: 45,}} source = {require('../UI/records.png')}>
                        </Image>
                    </TouchableOpacity>
                    <TouchableOpacity style = {{ margin: 8, backgroundColor: 'white', height: 70, width: 70,  borderRadius: 45, justifyContent: 'center', alignSelf: 'center', shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34, shadowRadius: 6.27, elevation: 10,}}>
                        {/* <Image style = {{ height: 50, width: 50 , alignSelf: 'center',}} source = {this.state.cupToUse}>
                        </Image> */}
                        {/* <Text style = {{justifyContent: 'center', alignSelf: 'center', fontSize: 15}}>
                        {'History'}
                        </Text> */}
                        {/* <Icon type= 'EvilIcons' name="ei-chart" /> */}
                        <Image style = {{ justifyContent: 'center', alignSelf: 'center', height: 45, width: 45,}} source = {require('../UI/history.png')}>
                        </Image>
                    </TouchableOpacity>
                    <TouchableOpacity style = {{ margin: 8, backgroundColor: 'white', height: 70, width: 70,  borderRadius: 45, justifyContent: 'center', alignSelf: 'center', shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34, shadowRadius: 6.27, elevation: 10,}} >
                        {/* <Image style = {{ height: 50, width: 50 , alignSelf: 'center',}} source = {this.state.cupToUse}>
                        </Image> */}
                        {/* <Text style = {{justifyContent: 'center', alignSelf: 'center', fontSize: 15}}>
                        {"Settings"}
                        </Text> */}
                        <Image style = {{ justifyContent: 'center', alignSelf: 'center', height: 65, width: 65,}} source = {require('../UI/settings.png')}>
                        </Image>
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {this.renderNotification.bind(this)} style = {{ margin: 8, backgroundColor: 'white', height: 70, width: 70,  borderRadius: 45, justifyContent: 'center', alignSelf: 'center', shadowColor: "#000", shadowOffset: { width: 0, height: 5, }, shadowOpacity: 0.34, shadowRadius: 6.27, elevation: 10,}}>
                        {/* <Image style = {{ height: 50, width: 50 , alignSelf: 'center',}} source = {this.state.cupToUse}>
                        </Image> */}
                       <Image style = {{ justifyContent: 'center', alignSelf: 'center', height: 50, width: 50,}} source = {require('../UI/notification.png')}>
                        </Image>
                    </TouchableOpacity>
                </View>
                <Modal
                    isVisible = {this.state.isModalVisible}
                    backdropOpacity = {0.40}
                    >
                    {this.selectAndSwitchToThisCup()}
                </Modal>
            </View>
        );

    }
}

export default Main;

const _styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        // backgroundColor: '',
    },
    wave: {
        height: 170,
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        aspectRatio: 1,
        overflow: 'hidden',
        backgroundColor: 'transparent',
        borderWidth: StyleSheet.hairlineWidth,

    },
    waveBall: {
        width: 200,
        aspectRatio: 1,
        borderRadius: 100,
        overflow: 'hidden',
        marginTop: 30,
        borderWidth: 1,
        borderColor: 'gray',
        alignSelf: 'center',
      
    }
});
